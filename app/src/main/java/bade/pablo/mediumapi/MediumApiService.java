package bade.pablo.mediumapi;

/**
 * Created by LC1300XXXX on 25/10/2017.
 */

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MediumApiService {

    @GET("loginAlumno")
    Call<MediumApiResponse> loginUsuario(@Query("Authorization") String token);

}
