package bade.pablo.mediumapi;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LC1300XXXX on 25/10/2017.
 */

public class MediumApiResponse {

    @SerializedName("data")
    private MediumApiUsuario usuario;

    public MediumApiResponse() {
    }

    public MediumApiUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(MediumApiUsuario usuario) {
        this.usuario = usuario;
    }
}
